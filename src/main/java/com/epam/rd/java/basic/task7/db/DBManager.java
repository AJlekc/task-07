package com.epam.rd.java.basic.task7.db;

import com.epam.rd.java.basic.task7.db.entity.Team;
import com.epam.rd.java.basic.task7.db.entity.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;


public class DBManager {

    private static final String URL = "jdbc:mysql://localhost:3306/test2db";
    private static final String USER = "root";
    private static final String PASSWORD = "12345";
    private static final String FULL_URL = URL + "?user=" + USER + "&password=" + PASSWORD + "&serverTimezone=UTC";


    private static final String GET_ALL_USERS = "SELECT * FROM users";
    private static final String GET_ALL_TEAMS = "SELECT * FROM teams";
    private static final String INSERT_NEW_USER = "INSERT INTO users(id,login)" + "VALUES(?, ?)";
    private static final String INSERT_NEW_TEAM = "INSERT INTO teams(id,name)" + "VALUES(?, ?)";
    private static final String INSERT_NEW_USERS_TEAMS = "INSERT INTO users_teams(user_id,team_id)" + "VALUES(?, ?)";


    private static DBManager instance;
    private Connection con;

    public static synchronized DBManager getInstance() {
        instance = new DBManager();
        return instance;
    }

    private DBManager() {
        try {
            con = DriverManager.getConnection(FULL_URL);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public List<User> findAllUsers() throws DBException {
        List<User> users = new ArrayList<>();
        try (Connection con = DriverManager.getConnection(FULL_URL)) {
            Statement statement = con.createStatement();
            ResultSet rs = statement.executeQuery(GET_ALL_USERS);
            while (rs.next()) {
                User user = new User();
                user.setId(rs.getInt("id"));
                user.setLogin(rs.getString("login"));
                users.add(user);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return users;
    }

    private void closeConnection(AutoCloseable stmt) {
        if (stmt != null) {
            try {
                stmt.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public boolean isPresentUser(User user) {
        Connection con = null;
        try {
            con = DriverManager.getConnection(FULL_URL);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        ;
        boolean isAvailable = false;
        String query = "SELECT * FROM users WHERE login = ?";
        try (PreparedStatement statement = con.prepareStatement(query)) {
            statement.setString(1, user.getLogin());
            try (ResultSet rs = statement.executeQuery()) {
                return rs.next();
            }
        } catch (SQLException se) {
            se.printStackTrace();
            return false;
        }

    }

    public boolean isPresentTeam(Team team) {
        Connection con = null;
        try {
            con = DriverManager.getConnection(FULL_URL);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        ;
        boolean isAvailable = false;
        String query = "SELECT * FROM teams WHERE name = ?";
        try (PreparedStatement statement = con.prepareStatement(query)) {
            statement.setString(1, team.getName());
            try (ResultSet rs = statement.executeQuery()) {
                return rs.next();
            }
        } catch (SQLException se) {
            se.printStackTrace();
            return false;
        }

    }

    public boolean insertUser(User user) throws DBException {
        if (isPresentUser(user)) return false;
        Connection con = null;
        PreparedStatement statement = null;
        boolean res = false;
        try {
            con = DriverManager.getConnection(FULL_URL);
            statement = con.prepareStatement(INSERT_NEW_USER, Statement.RETURN_GENERATED_KEYS);
            statement.setLong(1, user.getId());
            statement.setString(2, user.getLogin());
            long accountId = user.getId();
            if (accountId == 0) {
                statement.setNull(1, Types.INTEGER);
            } else {
                statement.setLong(2, accountId);
            }
            int count = statement.executeUpdate();
            res = true;
            if (count > 0) {
                try (ResultSet rs = statement.getGeneratedKeys()) {
                    if (rs.next()) {
                        user.setId(rs.getInt(1));
                    }
                }
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            closeConnection(statement);
            closeConnection(con);
        }

        return res;
    }

    public boolean deleteUsers(User... users) throws DBException {
        Connection con = null;
        PreparedStatement statement = null;
        boolean res = false;
        try {
            con = DriverManager.getConnection(FULL_URL);
            String sql = "DELETE FROM users where id = ? AND login = ?";
            statement = con.prepareStatement(sql);
            for (User user : users) {
                statement.setLong(1, user.getId());
                statement.setString(2, user.getLogin());
            }

            statement.execute();

            res = true;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            try {
                if (statement != null)
                    con.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (con != null)
                    con.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return res;
    }

    public User getUser(String login) throws DBException {
        Connection con = null;
        Statement statement = null;
        User user;
        try {
            con = DriverManager.getConnection(FULL_URL);
            statement = con.createStatement();
            String sql = "SELECT id, login FROM users WHERE login='" + login + "'";
            ResultSet rs = statement.executeQuery(sql);
            user = new User();
            while (rs.next()) {
                user.setId(rs.getInt("id"));
                user.setLogin(rs.getString("login"));
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            closeConnection(statement);
            closeConnection(con);
        }
        return user;
    }

    public Team getTeam(String name) throws DBException {
        Connection con = null;
        Statement statement = null;
        Team team;
        try {

            con = DriverManager.getConnection(FULL_URL);
            statement = con.createStatement();
            String sql = "SELECT id, name FROM teams WHERE name='" + name + "'";
            ResultSet rs = statement.executeQuery(sql);
            team = new Team();
            while (rs.next()) {
                team.setId(rs.getInt("id"));
                team.setName(rs.getString("name"));
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return team;
    }

    public List<Team> findAllTeams() throws DBException {
        List<Team> teams = new ArrayList<>();
        try (Connection con = DriverManager.getConnection(FULL_URL)) {
            Statement statement = con.createStatement();
            ResultSet rs = statement.executeQuery(GET_ALL_TEAMS);
            while (rs.next()) {
                Team team = new Team();
                team.setId(rs.getInt("id"));
                team.setName(rs.getString("name"));
                teams.add(team);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return teams;
    }

    public boolean insertTeam(Team team) throws DBException {
        if (isPresentTeam(team)) return false;
        Connection con = null;
        PreparedStatement statement = null;
        boolean res = false;
        try {
            con = DriverManager.getConnection(FULL_URL);
            statement = con.prepareStatement(INSERT_NEW_TEAM, Statement.RETURN_GENERATED_KEYS);
            statement.setLong(1, team.getId());
            statement.setString(2, team.getName());
            long accountId = team.getId();
            if (accountId == 0) {
                statement.setNull(1, Types.INTEGER);
            } else {
                statement.setLong(2, accountId);
            }
            int count = statement.executeUpdate();
            res = true;
            if (count > 0) {
                try (ResultSet rs = statement.getGeneratedKeys()) {
                    if (rs.next()) {
                        team.setId(rs.getInt(1));
                    }
                }
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            closeConnection(statement);
            closeConnection(con);
        }

        return res;
    }

    public boolean setTeamsForUser(User user, Team... teams) throws DBException {
        Connection conn = null;
        PreparedStatement pstmt = null;
        boolean res = false;

        try {
            conn = DriverManager.getConnection(FULL_URL);
            pstmt = conn.prepareStatement(INSERT_NEW_USERS_TEAMS);
            conn.setAutoCommit(false);

            for (Team team : teams) {
                pstmt.setInt(1, user.getId());
                pstmt.setLong(2, team.getId());

                pstmt.execute();
            }

            res = true;
            conn.commit();
        } catch (SQLException e) {
            try{
                if(conn != null)
                    conn.rollback();
            }catch(SQLException ex){
                System.out.println(ex.getMessage());
            }
        } finally {
            closeConnection(pstmt);
            closeConnection(con);
        }

        return res;
    }

    public List<Team> getUserTeams(User user) throws DBException {

        return null;
    }

    public boolean deleteTeam(Team team) throws DBException {
        Connection con = null;
        PreparedStatement statement = null;
        boolean res = false;
        try {
            con = DriverManager.getConnection(FULL_URL);
            String sql = "DELETE FROM teams where id = ? AND name = ?";
            statement = con.prepareStatement(sql);
            statement.setLong(1, team.getId());
            statement.setString(2, team.getName());

            statement.execute();

            res = true;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            try {
                if (statement != null)
                    con.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (con != null)
                    con.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return res;
    }

    public boolean updateTeam(Team team) throws DBException {
        Connection con = null;
        PreparedStatement preparedStatement = null;

        try {
            con = DriverManager.getConnection(FULL_URL);
            String query = "UPDATE teams SET id = ? AND name = ?";
            preparedStatement = con.prepareStatement(query);

            preparedStatement.setInt(1, team.getId());
            preparedStatement.setString(2, team.getName());

            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            closeConnection(preparedStatement);
            closeConnection(con);
        }
        return false;
    }

}
